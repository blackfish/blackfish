{
    "builders": [
        {
            "name": "aws",
            "type": "amazon-ebs",
            "region": "{{user `aws-region`}}",
            "source_ami": "{{user `aws-source-ami`}}",
            "instance_type": "m4.4xlarge",
            "ssh_username": "core",
            "ebs_optimized": "true",
            "ami_name": "blackfish-{{user `coreos-channel`}}-{{user `version`}}",
            "ami_groups" : "all",
            "ami_block_device_mappings": [
                 {
                     "device_name": "/dev/xvda",
                     "volume_size": "3"
                 }
            ],
            "launch_block_device_mappings": [
                {
                    "device_name": "/dev/xvdd",
                    "virtual_name": "ephemeral0"
                },
                {
                    "device_name": "/dev/xvde",
                    "virtual_name": "ephemeral1"
                },
                {
                    "device_name": "/dev/xvdf",
                    "virtual_name": "ephemeral2"
                },
                {
                    "device_name": "/dev/xvdg",
                    "virtual_name": "ephemeral3"
                }
            ]
        },
        {
            "name": "vbox",
            "type": "virtualbox-ovf",
            "source_path": "{{user `build_path`}}/{{user `ovf-source-path`}}",
            "ssh_username": "core",
            "output_directory": "{{user `build_path`}}/blackfish",
            "headless": true,
            "ssh_private_key_file": "./vagrant_private_key",
            "ssh_host_port_min": 2222,
            "ssh_host_port_max": 2229,
            "ssh_port": 22,
            "ssh_wait_timeout": "5m",
            "vm_name": "blackfish-packer",
            "shutdown_command": "sudo -S shutdown -P now",
            "vboxmanage": [
                [ "modifyvm", "{{.Name}}", "--memory", "1024" ],
                [ "modifyvm", "{{.Name}}", "--cpus", "1" ]
            ],
            "guest_additions_mode": "disable"
        },
        {
            "name": "vbox-release",
            "type": "virtualbox-ovf",
            "source_path": "{{user `build_path`}}/{{user `ovf-source-path`}}",
            "ssh_username": "core",
            "output_directory": "{{user `build_path`}}/blackfish",
            "headless": true,
            "ssh_private_key_file": "./vagrant_private_key",
            "ssh_host_port_min": 2222,
            "ssh_host_port_max": 2229,
            "ssh_port": 22,
            "ssh_wait_timeout": "5m",
            "vm_name": "blackfish-packer",
            "shutdown_command": "sudo -S shutdown -P now",
            "vboxmanage": [
                [ "modifyvm", "{{.Name}}", "--memory", "1024" ],
                [ "modifyvm", "{{.Name}}", "--cpus", "1" ]
            ],
            "guest_additions_mode": "disable"
        },
        {
            "name": "openstack",
            "type": "qemu",
            "iso_url": "file:///{{user `build_path`}}/coreos_production_openstack_image.img",
            "iso_checksum": "4a63e2055a0c3e16e6d26f427f3ec7e9",
            "iso_checksum_type": "MD5",
            "disk_image" : true,
            "disk_compression": true,
            "output_directory": "{{user `build_path`}}/coreos-openstack",
            "qemuargs": [[ "-m", "1024M"],
                         ["-virtfs","local,path={{user `build_path`}},mount_tag=config-2,security_model=passthrough,id=config-2"]],
            "format": "raw",
            "headless": true,
            "ssh_username": "core",
            "ssh_private_key_file" : "./vagrant_private_key",
            "ssh_host_port_min": 2222,
            "ssh_host_port_max": 2229,
            "ssh_port": 22,
            "ssh_wait_timeout": "20s",
            "vm_name": "coreos-packer",
            "net_device": "virtio-net",
            "disk_interface": "virtio",
            "shutdown_command": "sudo -S shutdown -P now"
        }

    ],
    "provisioners": [
        {
            "type": "shell",
            "inline": ["echo CLEANUP={{user `cleanup`}} && mkdir -p /home/core/tmp"]
        },
        {
            "type": "file",
            "source": "{{user `build_path`}}/blackfish-{{user `version`}}.tar",
            "destination" : "/home/core/tmp/blackfish.tar"
        },
        {
            "type": "file",
            "source": "{{user `build_path`}}/docker-images/",
            "destination" : "/home/core/tmp/",
            "only": ["vbox", "vbox-release", "openstack"]
        },
        {
            "type": "shell",
            "inline_shebang" : "/bin/bash -xe",
            "environment_vars" : ["WORKDIR=/home/core/tmp", "CLEANUP={{user `cleanup`}}"],
            "inline": ["cd /home/core/tmp; tar -xf ./blackfish.tar && ./blackfish-{{user `version`}}/install.sh"]
        },
        {
            "type": "shell",
            "inline": ["rm -Rf /home/core/tmp"]
        }
    ],
    "post-processors": [
        [
            {
                "type": "vagrant",
                "only": ["vbox", "vbox-release"],
                "output": "{{user `build_path`}}/blackfish-coreos.box",
                "keep_input_artifact": true,
                "vagrantfile_template": "./builds/coreos-{{user `coreos-channel`}}/Vagrantfile",
                "include": [
                    "./builds/coreos-{{user `coreos-channel`}}/base_mac.rb",
                    "./builds/coreos-{{user `coreos-channel`}}/change_host_name.rb",
                    "./builds/coreos-{{user `coreos-channel`}}/configure_networks.rb"
                ]
            },
            {
                "type": "atlas",
                "only": ["vbox-release"],
                "token": "{{user `atlas_token`}}",
                "artifact": "yanndegat/blackfish",
                "artifact_type": "vagrant.box",
                "metadata": {
                    "created_at": "{{timestamp}}",
                    "provider": "virtualbox",
                    "version": "{{user `version`}}"
                }
            },
            {
                "type": "atlas",
                "only": ["openstack"],
                "token": "{{user `atlas_token`}}",
                "artifact": "yanndegat/blackfish",
                "artifact_type": "openstack.image",
                "metadata": {
                    "created_at": "{{timestamp}}",
                    "provider": "openstack",
                    "version": "{{user `version`}}"
                }
            }

        ]
    ],
    "variables": {
        "aws-region": "eu-west-1",
        "coreos-channel": "{{env `COREOS_CHANNEL`}}",
        "aws-source-ami": "{{env `COREOS_AMI`}}",
        "aws-account-id": "{{env `AWS_ACCOUNT_ID`}}",
        "ovf-source-path": "coreos-{{env `COREOS_CHANNEL`}}/box.ovf",
        "atlas_token": "{{env `ATLAS_TOKEN`}}",
        "build_path": "{{env `PWD`}}/builds",
        "docker_images": "{{env `DOCKER_IMAGES`}}",
        "cleanup": "{{env `CLEANUP`}}",
        "version": "{{env `VERSION`}}"
    }
}
