resource "openstack_networking_secgroup_v2" "bastion_sg" {
  name = "${var.stack_name}_bastion_sg"
  description = "blackfish bastion sg"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_from_all_vpn_traffic" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 1194
  port_range_max = 1194
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_all_vpn_traffic" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 1194
  port_range_max = 1194
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_from_all_ssh_traffic" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 22
  port_range_max = 22
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_all_ssh_traffic" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 22
  port_range_max = 22
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_from_nodes_all_tcp_traffic" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 1
  port_range_max = 65535
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_from_nodes_all_udp_traffic" {
  direction = "ingress"
  ethertype = "IPv4"
  port_range_min = 1
  port_range_max = 65535
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gateway_all_tcp_traffic" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 1
  port_range_max = 65535
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "gateway_all_udp_traffic" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 1
  port_range_max = 65535
  protocol = "udp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_ip_prefix = "0.0.0.0/0"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_nodes_control" {
  direction = "egress"
  ethertype = "IPv4"
  port_range_min = 4443
  port_range_max = 4443
  protocol = "tcp"
  security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
  remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_consul_cluster_tcp" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 8300
    port_range_max = 8302
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_consul_cluster_udp" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 8300
    port_range_max = 8302
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_consul_agent" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 8500
    port_range_max = 8501
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_consul_dns_tcp" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 8600
    port_range_max = 8600
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_consul_dns_udp" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 8600
    port_range_max = 8600
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_dns_tcp" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 53
    port_range_max = 53
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_dns_udp" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 53
    port_range_max = 53
    protocol = "udp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_swarm_manager" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 4000
    port_range_max = 4000
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "bastion_to_docker_registry" {
    direction = "egress"
    ethertype = "IPv4"
    port_range_min = 5000
    port_range_max = 5000
    protocol = "tcp"
    security_group_id = "${openstack_networking_secgroup_v2.bastion_sg.id}"
    remote_group_id = "${openstack_networking_secgroup_v2.nodes_sg.id}"
}
