provider "openstack" {
  user_name   = "${var.os_username}"
  password    = "${var.os_password}"
  tenant_name = "${var.os_tenant_name}"
  tenant_id   = "${var.os_tenant_id}"
  auth_url    = "${var.os_auth_url}"
}

