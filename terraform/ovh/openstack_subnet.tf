resource "openstack_networking_subnet_v2" "bf_subnet" {
  network_id  = "${var.os_net_id}"
  region      = "${var.ovh_region}"
  cidr        = "${lookup(var.cidr24_priv_block, var.ovh_region)}"
  enable_dhcp = true
  dns_nameservers = [ "8.8.8.8", "8.8.4.4" ]
}
